import socket

serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serversocket.bind(('localhost', 1984))
serversocket.listen(5)

print "the device is ready to receive data"
print "==================================="
while True:
    labels = ['UTC of Position', 'Latitude', 'N or S', 'Longitude','E or W',
	'GPS quality indicator (0=invalid; 1=GPS fix; 2=Diff. GPS fix)',
 	'Number of satellites in use [not those in view]','Horizontal dilution of position',
 	'Antenna altitude above/below mean sea level (geoid)',	'Meters  (Antenna height unit)',
	'Geoidal separation (Diff. between WGS-84 earth ellipsoid and \n mean sea level.  -=geoid is below WGS-84 ellipsoid)',
	'Meters  (Units of geoidal separation)',
	'Age in seconds since last update from diff. reference station',
	'Diff. reference station ID#',
	'Checksum']
    connection, address = serversocket.accept()
    buf = connection.recv(128)
    if buf != 'Exit':
        print "processing....."
        # print buf
        nmea_list = [str(x) for x in buf.split(',')]
        # print nmea_list
        if len(nmea_list) != 15:
            print "ERROR::The stream provided is not a valid NMEA stream. Kindly format it correctly"
            break
        else:
            for x in xrange(0,15):
            	print x+1,'. ',labels[x],' = ',nmea_list[x]
            print "--- Done. Ready to process next stream ---"
    else:
        print "Exiting. Thank you."
        break
