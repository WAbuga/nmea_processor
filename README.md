## GPS Gadget assignment
# This README file describes how to run the simple gadget simulation
### Running system on linux system 

1. download the repository from, [download nmea_processor](https://bitbucket.org/WAbuga/nmea_processor). A direct download link is [zip download](https://bitbucket.org/WAbuga/nmea_processor/get/10fc8ec979d0.zip) (also available on the right panel)
  - **Note:** You will require Python 2.7.3
2. Extract the file and navigate into the directory `cd /path/to/nmea_processor/`
3. Inside this directory run the following command  `python gps_gadget.py`. This will simulate the gadget that will wait for a stream to be sent.
4. Open a new terminal window and navigate to the same directory as above. Run the following command  `python gps_stream.py '$GPGGA,092750.000,5321.6802,N,00630.3372,W,1,8,1.03,61.7,M,55.2,M,,*76'`. This command will send the string argument to the gadget.
5. Go back to the terminal window opened in step 2 above and view the output. You should see something as below:
```
the device is ready to receive data
===================================
processing.....
1 .  UTC of Position  =  $GPGGA
2 .  Latitude  =  092750.000
3 .  N or S  =  5321.6802
4 .  Longitude  =  N
5 .  E or W  =  00630.3372
6 .  GPS quality indicator (0=invalid; 1=GPS fix; 2=Diff. GPS fix)  =  W
7 .  Number of satellites in use [not those in view]  =  1
8 .  Horizontal dilution of position  =  8
9 .  Antenna altitude above/below mean sea level (geoid)  =  1.03
10 .  Meters  (Antenna height unit)  =  61.7
11 .  Geoidal separation (Diff. between WGS-84 earth ellipsoid and 
 mean sea level.  -=geoid is below WGS-84 ellipsoid)  =  M
12 .  Meters  (Units of geoidal separation)  =  55.2
13 .  Age in seconds since last update from diff. reference station  =  M
14 .  Diff. reference station ID#  =  
15 .  Checksum  =  *76
--- Done. Ready to process next stream ---

```

# The gadget simply reformats the GPGGA stream data into a human readable format. You can change the arguments and rerun the gps_stream.py to see a different output. 
6. To stop the gadget, send the following stream from the terminal window opened in step 4 above `python gps_stream.py Exit`

